import pandas as pd
import re
import sys
import numpy as np
import pylab as pl
import time
import os
import itertools
import traceback
from sklearn import svm, cross_validation, linear_model
#import pyparsing
from tkinter import *
import tkinter as tk
from tkinter.filedialog import askopenfilenames
import tkinter.scrolledtext as tkScrollable
from collections import defaultdict
from collections import Counter
from difflib import SequenceMatcher
from operator import itemgetter
from os.path import basename
from sklearn.cluster import KMeans

try:
    startTime = time.time()
    global fileCounter
    fileCounter = 0


    def openfiledirectory():
        root.fileName = askopenfilenames()
        root.destroy()


    def closewindowandexitprogram():
        root.destroy()
        sys.exit("Application Closed By User")


    def incrementview():
        global fileCounter
        fileCounter += 1
        if fileCounter >= len(combinationList):
            fileCounter = 0
        print("File Counter :",fileCounter)
        print("File Path Length :", len(filepathString))
        print("Len CombinationList", len(combinationList))
        print(combinationList)
        print(combinationList[fileCounter])
        print(filepathString[combinationList[fileCounter][0]])
        print(filepathString[combinationList[fileCounter][1]])
        firstTextWindow.delete('1.0', END)
        secondTextWindow.delete('1.0', END)
        with open(filepathString[combinationList[fileCounter][0]], "r") as file:
            firstTextWindow.insert(END, file.read())
        with open(filepathString[combinationList[fileCounter][1]], "r") as file:
            secondTextWindow.insert(END, file.read())


    def decrementview():
        global fileCounter
        print("File Counter :", fileCounter)
        fileCounter -= 1
        if fileCounter <= 0:
            print("Here")
            fileCounter = len(combinationList)
        print("File Counter :", fileCounter)
        print("File Path Length :", len(filepathString))
        print("Len CombinationList", len(combinationList))
        print(combinationList)
        print(combinationList[fileCounter])
        print(filepathString[combinationList[fileCounter][0]])
        print(filepathString[combinationList[fileCounter][1]])
        firstTextWindow.delete('1.0', END)
        secondTextWindow.delete('1.0', END)
        with open(filepathString[combinationList[fileCounter][0]], "r") as file:
            firstTextWindow.insert(END, file.read())
        with open(filepathString[combinationList[fileCounter][1]], "r") as file:
            secondTextWindow.insert(END, file.read())



    root = Tk()
    root.title("Plagiarism Detection System")

    titleLabel = Label(root, text="Plagiarism Detection System", font=("Calibri", 20))
    titleLabel.pack()
    instructionLabel = Label(root, text="Once Start Has Been Pressed A Directory Will Be Shown To Allow You to Select Files",font=("Calibri", 12))
    instructionLabel.pack()
    startButton = Button(root, text="Start", font=("Calibri", 12), command=openfiledirectory)
    startButton.pack()
    exitButton = Button(root, text="Exit", font=("Calibri", 12), command=closewindowandexitprogram)
    exitButton.pack()
    mainloop()






    filepathString = root.fileName
    #filepathString = sys.argv[1:]
    filepathCount = len(filepathString)
    fileInfoDictionary = defaultdict(list)
    FileList = defaultdict(list)
    GraphList = defaultdict(list)
    similarityArray = defaultdict(list)
    stringList = defaultdict(list)
    stringFrequency = defaultdict(list)
    refineStringFrequency = defaultdict(list)
    inverseStringFrequency = defaultdict(list)
    refineInverseStringFrequency = defaultdict(list)
    tdidfValue = defaultdict(list)
    unsplitFileList = []
    wordToBeHighlighted = []
    tokenFrequency = []
    finalTokenList = []
    stringLength = []
    testingList = []
    testingList2 = defaultdict(list)
    refinedTFIDFlist = defaultdict(list)
    refinedList = defaultdict(list)
    minMaxTFIDF = []




    ##Read in all Files which have been selcted by the user and insert them in data frames.
    for y in range(0, filepathCount):
        print("Files Being Compared :")
        print(basename(filepathString[y]))
        data = pd.read_fwf(filepathString[y])
        #print(data)
        #n = 300
        #data = data[0:n]
        #print(len(data.columns))
        #data.columns = ['a']

        startingDir = 'C:\\'
        fileTypesAllowed = [('Class File', "*.class")]
        title = "Please Choose Files"

        datanew = data.iloc[:, 0]
        datanew = datanew.dropna()

        TotalRows = len(datanew.index)

        wordImport = 'import'
        wordIf = 'if'
        wordFor = 'for'
        wordComment = '//'
        wordArray = '[]'

        counterImport = 0
        counterIf = 0
        counterFor = 0
        counterComment = 0
        counterArray = 0

        lista = []
        unsplitFileList.append(datanew.values)
        ##Counter to check how many times a word appear
        ##To be removed
        for x in range(0, TotalRows):

            lista.extend(datanew.values[x].split(" "))

            if wordImport in datanew.values[x]:
             counterImport += 1
            if wordIf in datanew.values[x]:
             counterIf += 1
            if wordFor in datanew.values[x]:
             counterFor += 1
            if wordComment in datanew.values[x]:
             counterComment += 1
            if wordArray in datanew.values[x]:
             counterArray += 1

        #print("Import Count : ", counterImport)
        #print("If Count : ", counterIf)
        #print("For Count : ", counterFor)
        #print("Comment Count : ", counterComment)
        #print("Array Count : ", counterArray)
        countTotal = counterImport+counterIf+counterFor+counterComment+counterArray
        #print("Total Count : ", countTotal)

        CountArray = [countTotal, counterImport, counterIf, counterFor, counterComment, counterArray]
        fileInfoDictionary[y].append({countTotal, counterImport, counterIf, counterFor, counterComment, counterArray})

        countingStrings = Counter(lista)
        countingStringLength = len(lista)
        FileList[y].append(Counter(lista))
        counter = Counter(lista)
        ##First part of TD-IDF
        ##Calculating the term frequency by finding out how many times the word appear and dividing it by the total under of words in document.
        print(counter)
        for value in counter:
            stringFrequency[value].append(counter[value] / len(counter))
        testingList.append(counter)





    print(stringFrequency)

    print("String" , testingList)
    print("Keys",testingList[0].values())

    #Importing the text file which includes of the Java's reserved words.
    with open(os.path.abspath("JavaReservedWords.txt")) as JavaReservedWordsFile:
        JavaReservedWords = JavaReservedWordsFile.readlines()
        #Removing the \n from the reserved words file
        JavaReservedWords = [words.strip() for words in JavaReservedWords]

    for x in range(0, len(testingList)):
        for key,value in zip(testingList[x].keys(),testingList[x].values()):
            templist = []
            idfValue = filepathCount / (1 + len(testingList))
            tf = value / len(testingList[x].values())
            templist.append(key)
            templist.append(tf * idfValue)
            #testingList2[x].append(key)

            testingList2[x].append(templist)
            #testingList2.extend(value / len(testingList[x].values()))

    #Finding the smallest value to be used as vector length
    VectorAmountArray = []
    for x in range(0, len(testingList2)):
        VectorAmountArray.append(len(testingList2[x]))

    VectorArraySize = min(VectorAmountArray)
    print("Vector Array Size :",min(VectorAmountArray))
    print("Vector Array Size :", VectorAmountArray)
    if(min(VectorAmountArray) & 1):
        VectorArraySize -= 1
    print(VectorArraySize)



    for x in range(0, len(testingList2)):
        Vectorcounter = 0
        for value in sorted(testingList2[x], key=itemgetter(1), reverse=True):

            if (re.search('[a-zA-Z]', value[0]) and value[0] not in JavaReservedWords and Vectorcounter < VectorArraySize):
                print(Vectorcounter, " ) ", value[0], value[1], value)
                Vectorcounter = Vectorcounter + 1
                refinedList[x].append(value)

        #refinedTFIDFlist[x].append(refinedList)

    #After the values have been checked against the REGEX and the reserved words list, check that the two arrays to check that they are the same size and an even number
    VectorAmountArray = []
    for x in refinedList:
        VectorAmountArray.append(len(refinedList[x]))
    VectorArraySize = min(VectorAmountArray)
    if(min(VectorAmountArray) & 1):
        VectorArraySize -= 1
    print("New Vector Array Size : ", VectorArraySize)
    for x in refinedList:
        refinedList[x] = refinedList[x][:VectorArraySize]


    combinationList = [list(y) for y in itertools.combinations(refinedList, 2)]
    print(combinationList)

    ApplicationWindow = Tk()
    textYScrollbar = Scrollbar(ApplicationWindow)
    #textXScrollbar = Scrollbar(ApplicationWindow)

    firstTextWindow = tkScrollable.ScrolledText(ApplicationWindow, height=50, width=110, wrap='word')
    firstTextWindow.grid(row=1, column=0)
    firstTextWindow.bind("<Key>", lambda e: "break")
    firstWindowButton = Button(ApplicationWindow, text ="Previous", command=decrementview)
    firstWindowButton.grid(row=2, column=0)
    firstWindowLabel = Label(ApplicationWindow, text=filepathString[0])
    firstWindowLabel.grid(row=0, column=0)
    with open(filepathString[0], "r") as file:
        firstTextWindow.insert(END, file.read())
    secondWindowLabel = Label(ApplicationWindow, text=filepathString[1])
    secondWindowLabel.grid(row=0, column=1)
    secondTextWindow = tkScrollable.ScrolledText(ApplicationWindow, height=50, width=110, wrap='word')
    secondTextWindow.grid(row=1, column=1)
    secondTextWindow.bind("<Key>", lambda e: "break")
    secondWindowButton = Button(ApplicationWindow, text ="Next", command=incrementview)
    secondWindowButton.grid(row=2, column=1)
    with open(filepathString[1], "r") as file:
        secondTextWindow.insert(END, file.read())


    #firstTextWindow = tkScrollable.ScrolledText(ApplicationWindow, height=50, width=110, wrap='word')
    #with open(filepathString[1], "r") as file:
    #    firstTextWindow.insert(END, file.read())
    #firstTextWindow.pack(side=LEFT)
    #secondTextWindow = tkScrollable.ScrolledText(ApplicationWindow, height=50, width=110, wrap='word')
    #with open(filepathString[1], "r") as file:
    #    secondTextWindow.insert(END, file.read())
    #secondTextWindow.pack(side=RIGHT)



    #textFormat = Text(ApplicationWindow, height=50, width=110)
    #secondDocFormat = Text(ApplicationWindow, height=50, width=110, )
    #textFormat.pack(side=LEFT, fill=Y)
    #secondDocFormat.pack(side=RIGHT, fill=Y)
    #with open(filepathString[1], "r") as file:
        #secondDocFormat.insert(END, file.read())
    #secondDocYScrollBar = Scrollbar(ApplicationWindow)
    #secondDocYScrollBar.pack(side=RIGHT, fill=Y)
    #secondDocYScrollBar.config(command=secondDocFormat.yview)
    #secondDocYScrollBar = Scrollbar(ApplicationWindow)
    #secondDocYScrollBar.pack(side=RIGHT, fill=X)
    #secondDocYScrollBar.config(command=secondDocFormat.xview)

    #textYScrollbar.pack(side=LEFT, fill=Y)
    #textYScrollbar.config(command=textFormat.yview)
    #textXScrollbar.pack(side=BOTTOM, fill=X)
    #textXScrollbar.config(command=textFormat.xview)
    #with open(filepathString[0], "r") as file:
       # textFormat.insert(END, file.read())
    #mainloop()


    comboCounter = 0

    clusteringList = []
    for x in combinationList:

        for value1, value2 in zip(refinedList[x[0]], refinedList[x[1]]):
            print(value1[0],value1[1],value2[0], value2[1],abs(value1[1] - value2[1]), x[0], x[1])
            tempclusteringList = []
            tempclusteringList.append(value1[1])
            tempclusteringList.append(value2[1])
            TDIDFtemplist = []
            TDIDFtemplist.append(abs(value1[1] - value2[1]))
            TDIDFtemplist.append(value1[0])
            TDIDFtemplist.append(value2[0])
            TDIDFtemplist.append(x[0])
            TDIDFtemplist.append(x[1])
            refinedTFIDFlist[comboCounter].append(TDIDFtemplist)
            clusteringList.append(tempclusteringList)
        comboCounter = comboCounter + 1
           # for val1, val2 in zip(value1[0], value2[0]):
                #print(val1, val2)

    #K Means Clustering
    #4 Clusters being created and each vector is being assigned to a cluster.

    kmeansClustering = KMeans(n_clusters=4,random_state=0).fit_predict(clusteringList)
    print("KmeansClustering", kmeansClustering)
    print("KmeansClustering", len(kmeansClustering))
    # Cluster number is then being assign to refindTFIDTlist
    print("TF-IDF Value, File 1 Word, File 2 Word, File Number, File Number, Cluster Number")

    for val in refinedTFIDFlist:
        print(len(refinedTFIDFlist[val]))
        for value,value1 in zip(refinedTFIDFlist[val],kmeansClustering):
            value.append(value1)
            print("TT", value)


    for x in refinedTFIDFlist:
        for y in range(0, len(refinedTFIDFlist[x]),2):
            list1 = refinedTFIDFlist[x][y]
            list2 = refinedTFIDFlist[x][y+1]
            print(y, list1, list2)
            minMaxTFIDF.append(list1[0])
            minMaxTFIDF.append(list2[0])

    print("Min",min(minMaxTFIDF))
    print("Max",max(minMaxTFIDF))

    #for x in range(0, len(refinedTFIDFlist)):

        #for y in list(refinedTFIDFlist[x]):
            #for val in y[x]:
                #   print(val)
         #print("SS")




    ##Create a refined list of stringFrequency by removing anything which doesnt appear in both of the documents.
    ##This is being done by checking the length of stringFrequency
    for v in stringFrequency:
        refineStringFrequency[v].append(stringFrequency[v])


    #Capturing All Values which have been renamed. Only values which have a similarity Ratio of > 0.5 will be considered.
    ##This has been put in place so that renamed words will still be considered for exam output will be compared to outputString
    counterv = 0
    for value in list(FileList):
        list1 = FileList[value]
        list2 = FileList[value+1]
        for key,val in zip(list1, list2):
            for k, v in zip(key, val):
                if (SequenceMatcher(None, k, v).ratio() > 0.5 and SequenceMatcher(None, k, v).ratio() < 1):
                    print(k,v)
                    if not(v in refineStringFrequency.keys()):
                        #print(k, v)
                        listtemp = []
                        listtemp.append(key.get(k)/len(key))
                        listtemp.append(val.get(v)/len(val))
                        counterv = counterv + 1
                        countervalue = len(refineStringFrequency) + counterv
                        refineStringFrequency[v].append(listtemp)


    ##This for loop is calculating the inverse document frequency which is taking the total number of files and dividing by the number of files which the word appears in.
    ##Plus 1 has been added so if a file appears in 0 documents, you are not dividing by zero.
    for refined in refineStringFrequency:
        #print(inverseStringFrequency[refined])
        for refine in refineStringFrequency[refined]:
            for q in range(0, len(refine)):
                value = filepathCount / (1 + len(stringFrequency[v]))
                tdidfValue[refined].append(refine[q] * value)

    ##Output Statements
    ##Displaying the total amount of matches
    print("There is a total of", len(tdidfValue), "matches in the", filepathCount, "files")



    counter = 0
    #pl.xlabel(basename(filepathString[0]) + " (TF IDF Value)")
    #pl.ylabel(basename(filepathString[1]) + " (TF IDF Value)")
    #xAxisNpArray = np.array(list(zip(*tdidfValue.values()))[0])
    #yAxisNpArray = np.array(list(zip(*tdidfValue.values()))[1])
    #pl.scatter(xAxisNpArray, yAxisNpArray, c='black', s=5)
    #print(list(zip(*tdidfValue.values()))[0])
    #print(list(zip(*tdidfValue.values()))[1])
    #print(np.max(xAxisNpArray))
    #print(np.max(yAxisNpArray))
    #print(np.min(xAxisNpArray))
    #print(np.min(yAxisNpArray))
    #if(0.0 > np.max(yAxisNpArray) ):
        #print("Hello")
    #noOfClusters = 4

    #xRandomCentroidPoint = np.random.uniform(np.min(xAxisNpArray), np.max(xAxisNpArray), size=noOfClusters)
    #yRandomCentroidPoint = np.random.uniform(np.min(xAxisNpArray), np.max(yAxisNpArray), size=noOfClusters)
    #XY = np.array(list(zip(xRandomCentroidPoint,yRandomCentroidPoint)), dtype=np.float32)
    #print(XY)

    noOfClusters = 4
    print("MM", minMaxTFIDF)
    print("gg", refinedTFIDFlist)


    #kmeansClusterArray.fit_predict(tempArray[0] for tempArray in numpyRefinedArray)
    #print("KMEANS", kmeansClusterArray)
    randomCentroidPoint = np.random.uniform(np.min(minMaxTFIDF), np.max(minMaxTFIDF), size=noOfClusters)
    print("Centroid", randomCentroidPoint)
    randomCentroidPointArray = defaultdict(list)

    #for x in refinedTFIDFlist:
        #for y in range(0, len(refinedTFIDFlist[x]),2):
            #list1 = refinedTFIDFlist[x][y]
            #list2 = refinedTFIDFlist[x][y+1]
            #minMaxTFIDF.append(list1[0])
            #minMaxTFIDF.append(list2[0])
    for x in refinedTFIDFlist:
        for y in range(0, len(refinedTFIDFlist[x])):
            list1 = refinedTFIDFlist[x][y]
            temp = []

            for idx, val in enumerate(randomCentroidPoint):
                print(idx, val, list1[0],abs(val - list1[0]))
                temp2 = []
                temp.append(abs(val - list1[0]))
                temp2.append(abs(val - list1[0]))
                temp2.append(temp.index(min(temp)) + 1)
                temp2.append(list1[1])
                temp2.append(list1[2])
            #randomCentroidPointArray[temp.index(min(temp)) + 1].append(abs(val - list1[0]))
            #randomCentroidPointArray[temp.index(min(temp)) + 1].append(temp.index(min(temp)) + 1)
            #randomCentroidPointArray[temp.index(min(temp)) + 1].append(list1[1])
            randomCentroidPointArray[temp.index(min(temp)) + 1].append(temp2)
            print("Min", min(temp), temp.index(min(temp)))


        #for value in refinedTFIDFlist[x]:
            #for v in value[0]:
                #print("V", v, value)
                #temp = []
                #for idx,val in enumerate(randomCentroidPoint):
                    #print(v, val, idx+1)
                    #temp.append(abs(val - v))
                #print("Min", min(temp), temp.index(min(temp))+1)
                #randomCentroidPointArray[temp.index(min(temp))+1].append(abs(val - v))
                #randomCentroidPointArray[temp.index(min(temp))+1].append(temp.index(min(temp))+1)

    print(randomCentroidPointArray)
    print("KK",refinedTFIDFlist)
    ratioArray = []
    firstWindowHighlightList = []
    secondWindowHighlightList = []
    number = 1
    for val in refinedTFIDFlist:
        for value in sorted(refinedTFIDFlist[val], key=itemgetter(5)):
            if(number != value[5]+1):
                print("\n Cluster", value[5]+1, "includes : \n")
                number = value[5]+1
            print(value[1], value[2])
            input = firstTextWindow.get("1.0",END)

            firstWindowHighlightList.append([x.start() for x in re.finditer(re.escape(value[1]), input)])
            secondWindowHighlightList.append([x.start() for x in re.finditer(re.escape(value[2]), input)])
            #print(input.count(value[1]))
            firstWindowPos = 1.0
            #adjustedString = "\W*(" + value[1] + ")\W*"
            #print(adjustedString)
            for x in range(0, input.count(value[1]) ):
                firstWindowPos = firstTextWindow.search(value[1], firstWindowPos, stopindex=END)
                firstWindowLastPos = '%s+%dc' % (firstWindowPos, len(value[1]))
                firstTextWindow.tag_add('cluster' + str(value[5] + 1), firstWindowPos, firstWindowLastPos)
                firstWindowPos = firstWindowLastPos

                firstTextWindow.tag_config('cluster1', foreground='red')
                firstTextWindow.tag_config('cluster2', foreground='yellow')
                firstTextWindow.tag_config('cluster3', foreground='blue')
                firstTextWindow.tag_config('cluster4', foreground='green')
            #print(re.finditer(value[1],input))
            #firstWindowPos = firstTextWindow.search(value[1], 1.0, stopindex=END)
            #firstWindowLastPos = END
            #print("First", firstWindowPos)
            #while firstWindowPos != firstWindowLastPos:
                #firstWindowLastPos = '%s+%dc' % (firstWindowPos, len(value[1]))
                #firstTextWindow.tag_add('cluster' + str(value[5] + 1), firstWindowPos, firstWindowLastPos)
                #firstWindowPos = firstWindowLastPos


            #secondWindowPos = secondTextWindow.search(value[2], 1.0, stopindex=END)
            #if secondWindowPos:
                #secondWindowLastPos = '%s+%dc' % (secondWindowPos, len(value[2]))
                #print("SecondWindowPos",secondWindowPos, secondWindowLastPos)
                #secondTextWindow.tag_add('cluster' + str(value[5]+1), secondWindowPos, secondWindowLastPos)
                #secondWindowPos = secondWindowLastPos


    #for value,value1,value2 in zip(firstWindowHighlightList,secondWindowHighlightList,sorted(refinedTFIDFlist[val], key=itemgetter(5))):
         #print("VV",value, value1, value2, len(value2[1]), value2[5])
         #input = firstTextWindow.get("1.0", END)
         #for v in value:
             #print(v)
             #firstTextWindowStartPos = firstTextWindow.index(str(v))
            # print(firstTextWindowStartPos)

    #
    #
    #     firstTextWindow.tag_config('cluster1', foreground='red')
    #     firstTextWindow.tag_config('cluster2', foreground='yellow')
    #     firstTextWindow.tag_config('cluster3', foreground='blue')
    #     firstTextWindow.tag_config('cluster4', foreground='green')
    #     secondTextWindow.tag_config('cluster1', foreground='red')
    #     secondTextWindow.tag_config('cluster2', foreground='yellow')
    #     secondTextWindow.tag_config('cluster3', foreground='blue')
    #     secondTextWindow.tag_config('cluster4', foreground='green')
    #for x in randomCentroidPointArray:
        #print("Cluster ", x, "Includes :", "\n")
       # for value in randomCentroidPointArray[x]:
           # pos = firstTextWindow.search(value[2], 1.0, stopindex=END)
            #if pos:
            #        lastPos = '%s+%dc' % (pos, len(value[2]))
            #        firstTextWindow.tag_add('cluster' + str(x), pos, lastPos)
            #        pos = lastPos
           # secondFilepos = secondTextWindow.search(value[3], 1.0, stopindex=END)
            #if secondFilepos:
            #        secondFileLastPos = '%s+%dc' % (secondFilepos, len(value[3]))
             #       secondTextWindow.tag_add('cluster' + str(x), secondFilepos, secondFileLastPos)
            #        secondFilepos = secondFileLastPos

            #print("Value",value)
            #print(value[2], value[3])

            #list1 = list(value)
            #print(list1[0])





    ##Displaying the top 15 words between the documents.
    for key, value in sorted(tdidfValue.items(), key=itemgetter(1), reverse=True):
        #print(Counter(value))
        ##Check word is within the alphabet, This is to remove values such as = or { }
        if(re.search('[a-zA-Z]', key)):
            counter = counter + 1

            #print(value[0], value[1])
            #pl.plot(value[0], value[1], 'ro')
            print(counter, ")",key, value)

            wordToBeHighlighted.append(key)
            if(counter == 100):
                break
        else:
            tdidfValue.pop(key, None)


    #print(tdidfValue.values())

    #pl.axis([0, 0.09, 0, 0.09])
    #pl.show()
    newList = []

    strTest = str(unsplitFileList)
    #print(unsplitFileList[0])

    #print(unsplitFileList[1])
    listString = ""
    rgxstr = "\{(.*?)\}"
    for rgxV in range(0, len(unsplitFileList)):
        #print(rgxV)
        listString = ''.join(unsplitFileList[rgxV])
        rgxString = re.findall(rgxstr, listString)
        newList.append(rgxString)
        #print(re.findall(rgxstr, listString))

    #print(newList)
    lexicalAnalysisList = []
    lexString = ""
    lisTest = []
    for listSize in newList:
        #print(listSize)
        for val in listSize:
            #print(val)
            for char in val:
                if char.isalpha():
                    lexString = lexString + "A"
                    #print(char, "A")
                if char.isdigit():
                    lexString = lexString + "D"
                    #print(char, "D")
                if char.isspace():
                    lexString = lexString + "W"
                    #print(char, "W")
                if char.isalnum() == False and char.isspace() == False:
                    lexString = lexString + "S"
                    #print(char, "S")
            #print("Lex", lexString)

            lexicalAnalysisList.append(lexString)
            #print(lexString)
            lexString = ""
            #print(len(lexicalAnalysisList))
            #print(lexicalAnalysisList)
        lisTest.append(lexicalAnalysisList)
        lexicalAnalysisList = []
    #print(len(lexicalAnalysisList))
    #print(lisTest)
    #print(len(lisTest))
    #print(lisTest)


    for y in range(0, len(lisTest)-1):
        list1 = lisTest[y]
        list2 = lisTest[y+1]
        nlist1 = newList[y]
        nlist2 = newList[y+1]
        #print(len(list1))
        #print(len(list2))
        for val1, val2, val3, val4 in zip(list1, list2, nlist1, nlist2):
            #for char in val1:

           #print(val1)
            #print(val2)
            if(SequenceMatcher(None, val1, val2).ratio() > 0.70):
                print(SequenceMatcher(None, val1, val2).ratio())
                print(val1)
                print(val2)
                print(val3)
                print(val4)
                #print(filepathString)

    #print(unsplitFileList)
    for x in unsplitFileList:
        for c in x:
            if(len(c) > 1):
                stringLength.append(len(c))

    totaltime = time.time() - startTime
    print("Execution Time : ", time.time() - startTime)
    print(os.path.abspath("ExecutionTime.txt"))
    executionTimeFile = open("C:/Users/Andrew/PycharmProjects/Project1/ExecutionTime.txt", "a")

    executionTimeFile.write("Execution Time : " + str(totaltime) + "\n")
    executionTimeFile.close()
    mainloop()

except Exception as exception:
    print("Error Logged to File ExceptionsRecord.txt")
    with open("C:/Users/Andrew/PycharmProjects/Project1/ExceptionsRecord.txt", "a") as ExceptionFile:
        ExceptionFile.write(traceback.format_exc() + "\n")


#print(stringLength)
#for y in range(0, 15):
   # print(Counter(stringLength))
