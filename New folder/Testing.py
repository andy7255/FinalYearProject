import pandas as pd
import re
from tkinter import Tk
from tkinter.filedialog import askopenfilenames
from collections import defaultdict
from collections import Counter
from difflib import SequenceMatcher
from operator import itemgetter


root = Tk()
root.fileName = askopenfilenames()

filepathString = root.fileName
filepathCount = len(filepathString)
fileInfoDictionary = defaultdict(list)
FileList = defaultdict(list)
GraphList = defaultdict(list)
similarityArray = defaultdict(list)
stringList = defaultdict(list)
stringFrequency = defaultdict(list)
refineStringFrequency = defaultdict(list)
inverseStringFrequency = defaultdict(list)
refineInverseStringFrequency = defaultdict(list)
tdidfValue = defaultdict(list)


##Read in all Files which have been selcted by the user and insert them in data frames.
for y in range(0, filepathCount):

    data = pd.read_fwf(root.fileName[y])
    #print(data)
    #n = 300
    #data = data[0:n]
    #print(len(data.columns))
    #data.columns = ['a']

    startingDir = 'C:\\'
    fileTypesAllowed = [('Class File', "*.class")]
    title = "Please Choose Files"

    datanew = data.iloc[:, 0]
    datanew = datanew.dropna()

    TotalRows = len(datanew.index)

    wordImport = 'import'
    wordIf = 'if'
    wordFor = 'for'
    wordComment = '//'
    wordArray = '[]'

    counterImport = 0
    counterIf = 0
    counterFor = 0
    counterComment = 0
    counterArray = 0

    lista = []

    ##Counter to check how many times a word appear
    ##To be removed
    for x in range(0, TotalRows):
        lista.extend(datanew.values[x].split(" "))

        if wordImport in datanew.values[x]:
         counterImport += 1
        if wordIf in datanew.values[x]:
         counterIf += 1
        if wordFor in datanew.values[x]:
         counterFor += 1
        if wordComment in datanew.values[x]:
         counterComment += 1
        if wordArray in datanew.values[x]:
         counterArray += 1

    #print("Import Count : ", counterImport)
    #print("If Count : ", counterIf)
    #print("For Count : ", counterFor)
    #print("Comment Count : ", counterComment)
    #print("Array Count : ", counterArray)
    countTotal = counterImport+counterIf+counterFor+counterComment+counterArray
    #print("Total Count : ", countTotal)

    CountArray = [countTotal, counterImport, counterIf, counterFor, counterComment, counterArray]
    fileInfoDictionary[y].append({countTotal, counterImport, counterIf, counterFor, counterComment, counterArray})

    countingStrings = Counter(lista)
    countingStringLength = len(lista)
    FileList[y].append(Counter(lista))
    counter = Counter(lista)
    ##First part of TD-IDF
    ##Calculating the term frequency by finding out how many times the word appear and dividing it by the total under of words in document.
    for value in counter:
        stringFrequency[value].append(counter[value] / len(counter))


##Create a refined list of stringFrequency by removing anything which doesnt appear in both of the documents.
##This is being done by checking the length of stringFrequency
for v in stringFrequency:

    if(len(stringFrequency[v])) > 1:
        refineStringFrequency[v].append(stringFrequency[v])

#Capturing All Values which have been renamed. Only values which have a similarity Ratio of > 0.5 will be considered.
##This has been put in place so that renamed words will still be considered for exam output will be compared to outputString
counterv = 0
for value in list(FileList):
    list1 = FileList[value]
    list2 = FileList[value+1]
    for key,val in zip(list1, list2):
        for k, v in zip(key, val):
            if (SequenceMatcher(None, k, v).ratio() > 0.5 and SequenceMatcher(None, k, v).ratio() < 1):
                if not(v in refineStringFrequency.keys()):
                    listtemp = []
                    listtemp.append(key.get(k)/len(key))
                    listtemp.append(val.get(v)/len(val))
                    counterv = counterv + 1
                    countervalue = len(refineStringFrequency) + counterv
                    refineStringFrequency[v].append(listtemp)

##This for loop is calculating the inverse document frequency which is taking the total number of files and dividing by the number of files which the word appears in.
##Plus 1 has been added so if a file appears in 0 documents, you are not dividing by zero.
for refined in refineStringFrequency:
    #print(inverseStringFrequency[refined])
    for refine in refineStringFrequency[refined]:
        for q in range(0, len(refine)):
            value = filepathCount / (1 + len(stringFrequency[v]))
            tdidfValue[refined].append(refine[q] * value)

##Output Statements
##Displaying the total amount of matches
print("There is a total of", len(tdidfValue), "matches in the", filepathCount, "files")

counter = 0

##Displaying the top 15 words between the documents.
for key, value in sorted(tdidfValue.items(), key=itemgetter(1), reverse=True):
    ##Check word is within the alphabet, This is to remove values such as = or { }
    if(re.search('[a-zA-Z]', key)):
        counter = counter + 1
        print(counter, ")",key, value)
        if(counter == 15):
            break
    else:
        tdidfValue.pop(key, None)