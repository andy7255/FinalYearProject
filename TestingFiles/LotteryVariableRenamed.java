import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

import javax.swing.JOptionPane;

public class Lottery
{
	private ArrayList<Integer> winningNumbers = new ArrayList<Integer>();
	private ArrayList<Bet> theBets = new ArrayList<Bet>();
	private int [] winningsArray = new int [6];
	private ArrayList<String> customerNames = new ArrayList<String>();

	private int profitLoss;
	private int prizeMoneyWon;


	final int minNumber = 1;
	final int maxNumber = 50;

	final int NUM_OF_PICKS = 6;

	final int maxTickets = 20;

	Random numGenerator;


	public Lottery()
	{
		this.profitLoss = 0;
		this.numGenerator = new Random();
		this.prizeMoneyWon = 0;
		
		this.winningsArray[0] = 3;
		this.winningsArray[1] = 6;
		this.winningsArray[2] = 25;
		this.winningsArray[3] = 750;
		this.winningsArray[4] = 5000;
		this.winningsArray[5] = 5000000;
	}

	public void play()
	{
		int num;
		do
		{
			String outputString = "Select one of the menu options \n\n";
			outputString = outputString + "1 - Place a bet by entering your own numbers\n";
			outputString = outputString + "2 - Place a bet by having the system pick numbers for you \n";
			outputString = outputString + "3 - Start the lottery draw \n";
			outputString = outputString + "4 - Close the application \n";

			String usersNumber = JOptionPane.showInputDialog(outputString);

			num = Integer.parseInt(usersNumber);
			if (num == 1)
			{
				outputString = "You have chosen to enter your own numbers, is this correct?";

				int numYesNoOptionSelected = JOptionPane.showConfirmDialog(null, outputString, "Alert", 1, JOptionPane.YES_NO_OPTION);
				if(numYesNoOptionSelected == JOptionPane.YES_OPTION){
					//Yes Selected
					betWithOwnNumbers();
				}
				else if(numYesNoOptionSelected == JOptionPane.NO_OPTION){
					//No Selected
					play();
					
				}

				
			}
			if (num == 2) {
				randomDraw();
			}
			if (this.theBets.size() == 20)
			{
				outputString = "The max number of bets have been placed. The lottery shall now begin.";
				display(outputString);
				num = 3;
			}
			if (num == 3)
			{
				drawLottery();
				displayResults();
				checkAllTickets();
				calculateProfitorLoss();
			}
			if (num != 4);

		} while (num != 4);
	}

	private void betWithOwnNumbers()
	{
		Boolean uniqueValue = Boolean.valueOf(true);

		String usersName = JOptionPane.showInputDialog("You have chosen to place your bet with your own numbers, please enter your name.");
		Bet newBet = new Bet(usersName);
		for (int loop = 0; loop < 6; loop++)
		{
			String usersNumber = JOptionPane.showInputDialog("Now enter a number between " + minNumber + " and " + maxNumber);
			int number = Integer.parseInt(usersNumber);
			while ((number < 1) || (number > 50))
			{
				usersNumber = JOptionPane.showInputDialog("The last number entered was invalid. The number must be between " + minNumber + " and " + maxNumber);
				number = Integer.parseInt(usersNumber);
			}
			if (loop > 0) {
				uniqueValue = Boolean.valueOf(!newBet.getTheNumbers().contains(Integer.valueOf(number)));
			}
			while ((loop > 0) && (!uniqueValue.booleanValue()))
			{
				usersNumber = JOptionPane.showInputDialog("The last number entered has already been chosen by an other player. Please choose another number between " + minNumber + " and " + maxNumber);
				number = Integer.parseInt(usersNumber);
				uniqueValue = Boolean.valueOf(!newBet.getTheNumbers().contains(Integer.valueOf(number)));
			}
			newBet.addNumber(number);
		}
		this.theBets.add(newBet);

		newBet.displayDetails();
	}

	private void randomDraw()
	{
		boolean uniqueValue = true;
		String usersName = JOptionPane.showInputDialog("Enter your name");
		customerNames.add(usersName);
		int custNameNumbofTickets = Collections.frequency(customerNames, usersName);
		while(custNameNumbofTickets >3){
			JOptionPane.showMessageDialog(null, "You cannot buy 3 or more tickets" + "\n" + "Returning to main menu", "Warning", JOptionPane.WARNING_MESSAGE);
			play();
			
		}
	
		Bet newBet = new Bet(usersName);
		
		System.out.println("Username : " + custNameNumbofTickets);
		
		
		for (int loop = 0; loop < 6; loop++)
		{
			int number = this.numGenerator.nextInt(50) + 1;
			if (loop > 0) {
				uniqueValue = !newBet.getTheNumbers().contains(Integer.valueOf(number));
			}
			while ((loop > 0) && (!uniqueValue))
			{
				number = this.numGenerator.nextInt(50) + 1;
				uniqueValue = !newBet.getTheNumbers().contains(Integer.valueOf(number));
			}
			System.out.println(number);
			newBet.addNumber(number);
		}
		this.theBets.add(newBet);

		newBet.displayDetails();
	}



	private void displayResults()
	{
		String outputString = "The lottery winning numbers are ";
		for (Integer tempInteger : this.winningNumbers) {
			outputString = outputString + tempInteger + "  ";
		}
		display(outputString);
	}

	private void drawLottery()
	{
		Boolean uniqueValue = Boolean.valueOf(true);
		for (int loop = 0; loop < 6; loop++)
		{
			int number = this.numGenerator.nextInt(50) + 1;
			if (loop > 0) {
				uniqueValue = Boolean.valueOf(!this.winningNumbers.contains(Integer.valueOf(number)));
			}
			while ((loop > 0) && (!uniqueValue.booleanValue()))
			{
				number = this.numGenerator.nextInt(50) + 1;
				uniqueValue = Boolean.valueOf(!this.winningNumbers.contains(Integer.valueOf(number)));
			}
			this.winningNumbers.add(Integer.valueOf(number));
		}
		Collections.sort(this.winningNumbers);
	}

	private void checkAllTickets()
	{
		for (Bet tempBet : this.theBets)
		{
			int numberOfMatches = 0;
			for (Integer number : tempBet.getTheNumbers()) {
				if (this.winningNumbers.contains(number)) {
					numberOfMatches++;
				}
			}
			String outputString = "Winning Numbers - " ;
			for (Integer tempInteger : this.winningNumbers) {
				outputString = outputString + tempInteger + "  ";
			}
			outputString += "\n" + tempBet.toString() + "\n";
			if (numberOfMatches == 0)
			{
				outputString = outputString + "This player found no matches \n";
			}
			else
			{
				this.prizeMoneyWon += this.winningsArray[numberOfMatches-1];
				
				outputString = outputString + "You have " + numberOfMatches + " match/matches";
				if (numberOfMatches > 1) {
					
				}
				
				outputString = outputString + ", and win " + this.winningsArray[numberOfMatches-1]+ " pounds";
			}
			display(outputString);
		}
	}

	private void calculateProfitorLoss()
	{
		this.profitLoss = (this.theBets.size() - this.prizeMoneyWon);

		String outputString = "With " + this.theBets.size() + " bets and " + this.prizeMoneyWon + " pounds paid out in prize money \n";
		outputString = outputString + "The lottery has made a ";
		if (this.profitLoss < 0) {
			outputString = outputString + "loss";
		}
		if (this.profitLoss == 0) {
			outputString = outputString + "break even";
		}
		if (this.profitLoss > 0) {
			outputString = outputString + "profit";
		}
		outputString = outputString + " of " + Math.abs(this.profitLoss) + " pounds";

		display(outputString);
	}

	private void display(String outputString)
	{
		JOptionPane.showMessageDialog(null, outputString, "Napiers Lottery", 1);
	}
}
