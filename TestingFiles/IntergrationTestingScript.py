import unittest
import os


class IntergrationTesting(unittest.TestCase):


    def test_upper(self):
        filepath = os.getcwd()
        #print("Hello", filepath)
        filepath = filepath + '/IntegrationTesting.txt'
        with open(filepath, 'r') as f:
            content = f.readlines()
        content = [x.strip() for x in content]
        assertFilePath = os.getcwd() +"/AssertOutput.txt"
        with open(assertFilePath, 'r') as file:
            assertContent = file.readlines()
        assertContent = [x.strip() for x in assertContent]
        counter = 0
        for x,y in zip(content,assertContent):
            if (counter <=437 or counter >=973 and counter <3034 or counter >=3038):
                print(counter, x, y)
                self.assertEqual(x, y)
            counter += 1


if __name__ == '_main_':
    unittest.main()