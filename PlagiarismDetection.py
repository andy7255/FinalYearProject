import pandas as pd
import re
import sys
import numpy as np
import pylab as pl
import time
import os
import itertools
import traceback
import Levenshtein
import inflect
from sklearn import svm, cross_validation, linear_model
#import pyparsing
from tkinter import *
import tkinter as tk
from tkinter.filedialog import askopenfilenames
import tkinter.scrolledtext as tkScrollable
from collections import defaultdict
from collections import Counter
from difflib import SequenceMatcher
from operator import itemgetter
from os.path import basename
from sklearn.cluster import KMeans
from scipy import spatial


try:
    startTime = time.time()
    global fileCounter
    fileCounter = 0

    #Method Which is called when the user clicks start
    #Method is in place to remove the start screen and display directory to select files
    def openfiledirectory():
        root.fileName = askopenfilenames()
        root.destroy()


    #Final Window
    #Final Window Screen is in place which will appear after the comparision screen to display the 4 cluster and the overview information
    def FinalWindow():
        #Remove Comparison screen
        ApplicationWindow.destroy()
        #Create Final Window
        resultWindow = Tk()

        #Cluster Windows
        cluster1Window = tkScrollable.ScrolledText(resultWindow, height=10, width=110, wrap='word')
        cluster1Window.grid(row=1, column=0)
        cluster2Window = tkScrollable.ScrolledText(resultWindow, height=10, width=110, wrap='word')
        cluster2Window.grid(row=3, column=0)
        cluster3Window = tkScrollable.ScrolledText(resultWindow, height=10, width=110, wrap='word')
        cluster3Window.grid(row=1, column=1)
        cluster4Window = tkScrollable.ScrolledText(resultWindow, height=10, width=110, wrap='word')
        cluster4Window.grid(row=3, column=1)
        finalResults = tkScrollable.ScrolledText(resultWindow, height=20, width=110, wrap='word')
        finalResults.grid(row=5, column=0,columnspan=1)
        colourfinalResults = tkScrollable.ScrolledText(resultWindow, height=20, width=110, wrap='word')
        colourfinalResults.grid(row=5, column=1, columnspan=1)
        LevenshteinWindowLabel = Label(resultWindow, text ="Files Overview", font=("Calibri", 12))
        LevenshteinWindowLabel.grid(row=4, column=0, columnspan=2)

        #Cluster Labels
        cluster1Label = Label(resultWindow, text="Cluster 1")
        cluster1Label.grid(row=0, column=0)
        cluster2Label = Label(resultWindow, text="Cluster 2")
        cluster2Label.grid(row=2, column=0)
        cluster3Label = Label(resultWindow, text="Cluster 3")
        cluster3Label.grid(row=0, column=1)
        cluster4Label = Label(resultWindow, text="Cluster 4")
        cluster4Label.grid(row=2, column=1)

        #Adding Information to the 4 cluster boxes
        clusterAverageList = defaultdict(list)
        for value in clusterArray[1]:
            tempString = "File :" + str(value[2]) + " " + str(value[0]) + " File : " + str(value[3])+ " " + str(value[1]) + "\n"
            cluster1Window.insert(END, tempString)
            clusterAverageList[1].append(SequenceMatcher(None, value[0], value[1]).ratio())
        for value in clusterArray[2]:
            tempString = "File :" + str(value[2]) + " " + str(value[0]) + " File : " + str(value[3])+ " " + str(value[1]) + "\n"
            cluster2Window.insert(END, tempString)
            clusterAverageList[2].append(SequenceMatcher(None, value[0], value[1]).ratio())
        for value in clusterArray[3]:
            tempString = "File :" + str(value[2]) + " " + str(value[0]) + " File : " + str(value[3])+ " " + str(value[1]) + "\n"
            cluster3Window.insert(END, tempString)
            clusterAverageList[3].append(SequenceMatcher(None, value[0], value[1]).ratio())
        for value in clusterArray[4]:
            tempString = "File :" + str(value[2]) + " " + str(value[0]) + " File : " + str(value[3])+ " " + str(value[1]) + "\n"
            cluster4Window.insert(END, tempString)
            clusterAverageList[4].append(SequenceMatcher(None, value[0], value[1]).ratio())

        #Adding Max and Average TF-IDF Values to Array
        for x in refinedList:
            tempAvg = []
            for value in refinedList[x]:
                tempAvg.append(value[1])
            maxTFIDFValue.append(np.amax(tempAvg))
            averageTFIDFValue.append(np.mean(tempAvg))
        #For loop which will add all of the information to the two File overview windows
        for x,y in zip(refinedList,combinationList):
            finalResults.insert(END, "Comparing Files: " + basename(filepathString[y[0]]) + " , " + basename(filepathString[y[1]])+ "\n")
            colourfinalResults.insert(END, "Comparing Files: " + basename(filepathString[y[0]]) + " , " + basename(
                filepathString[y[1]]) + "\n")
            print("Comparing Files :",basename(filepathString[y[0]]), ",", basename(filepathString[y[1]]))
            #Max and Mean Values being printed to Overview

            finalResults.insert(END, "Max TF-IDF Value : ")
            colourfinalResults.insert(END, "Max TF-IDF Value : ")
            sortedMaxTFIDF = sorted(maxTFIDFValue)
            sortedMaxTFIDF = list(set(sortedMaxTFIDF))
            MaxTFIDFIndex = sortedMaxTFIDF.index(maxTFIDFValue[x]) + 1
            colourfinalResults.insert(END, inflectengine.ordinal(MaxTFIDFIndex))
            colourfinalResults.insert(END, "\n")
            finalResults.insert(END, maxTFIDFValue[x])

            print("MAX", maxTFIDFValue)
            print("AVG", averageTFIDFValue)
            finalResults.insert(END, "\n")
            finalResults.insert(END, "Average TF-IDF Value : ")
            colourfinalResults.insert(END, "Average TF-IDF Value : ")
            sortedAVGTFIDF = sorted(averageTFIDFValue)
            sortedAVGTFIDF = list(set(sortedAVGTFIDF))
            AVGTDIDFIndex = sortedAVGTFIDF.index(averageTFIDFValue[x]) + 1
            colourfinalResults.insert(END, inflectengine.ordinal(AVGTDIDFIndex))
            colourfinalResults.insert(END, "\n")
            finalResults.insert(END, averageTFIDFValue[x])

            finalResults.insert(END, "\n")

            #Top 5 TF-IDF Values being printed to Overview
            top5 = refinedTFIDFlist[x][:5]
            finalResults.insert(END, "Top 5 TF-IDF Values")
            finalResults.insert(END, "\n")
            for z in top5:
                index = top5.index(z)+1
                finalResults.insert(END, index)
                finalResults.insert(END, " ) ")
                finalResults.insert(END, z[1])
                finalResults.insert(END, " , ")
                finalResults.insert(END, z[2])
                finalResults.insert(END, "\n")
            finalResults.insert(END, "Levenshtein Average : ")
            colourfinalResults.insert(END, "Levenshtein Average : ")
            sortedLevenshteinAverage = sorted(levenshteinAverageList)
            sortedLevenshteinAverage = list(set(sortedLevenshteinAverage))
            LevenshteinAverageIndex = sortedLevenshteinAverage.index(levenshteinAverageList[x]) + 1
            colourfinalResults.insert(END, inflectengine.ordinal(LevenshteinAverageIndex))
            colourfinalResults.insert(END, "\n")
            finalResults.insert(END, levenshteinAverageList[x])
            finalResults.insert(END, "\n")
            finalResults.insert(END, "Levenshtein Comparisons Made : ")
            colourfinalResults.insert(END, "Levenshtein Comparisons Made : ")
            sortedLevenshteinComparisons = sorted(levenshteinCombinationList)
            sortedLevenshteinComparisons = list(set(sortedLevenshteinComparisons))
            LevenshteinIndex = sortedLevenshteinComparisons.index(levenshteinCombinationList[x]) + 1
            colourfinalResults.insert(END, inflectengine.ordinal(LevenshteinIndex))
            colourfinalResults.insert(END, "\n")
            finalResults.insert(END, levenshteinCombinationList[x])
            finalResults.insert(END, "\n")
            finalResults.insert(END, "Files Cosine Value : ")
            colourfinalResults.insert(END, "Files Cosine Rank : ")
            sortedCosine = sorted(fileCosineValue)
            sortedCosine = list(set(sortedCosine))
            cosineindex = sortedCosine.index(fileCosineValue[x]) + 1
            colourfinalResults.insert(END, inflectengine.ordinal(cosineindex))
            colourfinalResults.insert(END, "\n")
            finalResults.insert(END, fileCosineValue[x])
            finalResults.insert(END, "\n")
            finalResults.insert(END, "Files Euclidean Value : ")
            sortedEuclidean = sorted(fileEuclideanValue)
            sortedEuclidean = list(set(sortedEuclidean))
            EuclideanValue = sortedEuclidean.index(fileEuclideanValue[x]) + 1
            colourfinalResults.insert(END, "Files Euclidean Value : ")
            colourfinalResults.insert(END, inflectengine.ordinal(EuclideanValue))
            colourfinalResults.insert(END, "\n")
            colourfinalResults.insert(END, "\n")
            finalResults.insert(END, fileEuclideanValue[x])

            finalResults.insert(END, "\n")
            finalResults.insert(END, "\n")

    #Exit button on start Screen
    #This will exit program completely and display message application closed by user
    def closewindowandexitprogram():
        root.destroy()
        sys.exit("Application Closed By User")

    #This Method is being used to cycle through the pairs of files in comparision screen

    def incrementview():
        global fileCounter
        fileCounter += 1
        if fileCounter >= len(combinationList):
            fileCounter = 0
        print("File Counter :",fileCounter)
        print("File Path Length :", len(filepathString))
        print("Len CombinationList", len(combinationList))
        print(combinationList)
        print(combinationList[fileCounter])
        print(filepathString[combinationList[fileCounter][0]])
        print(filepathString[combinationList[fileCounter][1]])
        firstTextWindow.delete('1.0', END)
        secondTextWindow.delete('1.0', END)

        print(firstWindowLabel)
        firstWindowLabel.configure(text=basename(filepathString[combinationList[fileCounter][0]]))
        secondWindowLabel.configure(text=basename(filepathString[combinationList[fileCounter][1]]))

        #Display the Class to the Windows
        with open(filepathString[combinationList[fileCounter][0]], "r") as file:
            firstTextWindow.insert(END, file.read())
        with open(filepathString[combinationList[fileCounter][1]], "r") as file:
            secondTextWindow.insert(END, file.read())

        firstWindowHighlightList = []
        secondWindowHighlightList = []
        number = 1
        #Displaying Information of What is in each cluster
        for val in refinedTFIDFlist:
            for value in sorted(refinedTFIDFlist[val], key=itemgetter(5)):
                if (number != value[5] + 1):
                    print("\n Cluster", value[5] + 1, "includes : \n")
                    number = value[5] + 1
                print(value[1], value[2])
                input = firstTextWindow.get("1.0", END)
                secondWindowInput = secondTextWindow.get("1.0", END)



                firstWindowHighlightList.append([x.start() for x in re.finditer(re.escape(value[1]), input)])
                secondWindowHighlightList.append([x.start() for x in re.finditer(re.escape(value[2]), input)])
                ##Highlighting the Words within the clusters
                if (value[3] == combinationList[fileCounter][0] and value[4] == combinationList[fileCounter][1]):
                    for val in FileList[value[3]]:
                        firstWindowPos = 1.0
                        secondWindowPos = 1.0
                        lenValue = val.get(value[1])
                        int(lenValue)
                        RegExpress = r'\W' + re.escape(value[1])
                        for x in range(0, lenValue):
                            firstWindowPos = firstTextWindow.search(RegExpress, firstWindowPos, stopindex=END, regexp=True)
                            firstWindowLastPos = '%s+%dc' % (firstWindowPos, len(value[1]) + 1)
                            firstTextWindow.tag_add("cluster" + str(value[5] + 1), firstWindowPos, firstWindowLastPos)
                            firstWindowPos = firstWindowLastPos

                            firstTextWindow.tag_config("cluster1", foreground='red')
                            firstTextWindow.tag_config("cluster2", foreground='yellow')
                            firstTextWindow.tag_config("cluster3", foreground='blue')
                            firstTextWindow.tag_config("cluster4", foreground='green')
                    for val in FileList[value[4]]:
                        secondWindowPos = 1.0
                        lenValue = val.get(value[2])
                        int(lenValue)
                        RegExpress = r'\W' + re.escape(value[2])
                        for x in range(0, lenValue):
                            secondWindowPos = secondTextWindow.search(RegExpress, secondWindowPos, stopindex=END,
                                                                  regexp=True)
                            secondWindowLastPos = '%s+%dc' % (secondWindowPos, len(value[2]) + 1)
                            secondTextWindow.tag_add("cluster" + str(value[5] + 1), secondWindowPos, secondWindowLastPos)
                            secondWindowPos = secondWindowLastPos

                            secondTextWindow.tag_config("cluster1", foreground='red')
                            secondTextWindow.tag_config("cluster2", foreground='yellow')
                            secondTextWindow.tag_config("cluster3", foreground='blue')
                            secondTextWindow.tag_config("cluster4", foreground='green')

    #Functionality for the Previous Button
    #This method will update the comparison screen when previous button is clicked
    def decrementview():
        global fileCounter
        print("File Counter :", fileCounter)
        fileCounter -= 1
        if fileCounter <= 0:
            print("Here")
            fileCounter = len(combinationList)-1
        print("File Counter :", fileCounter)
        print("File Path Length :", len(filepathString))
        print("Len CombinationList", len(combinationList))
        print(combinationList)
        print(combinationList[fileCounter])
        print(filepathString[combinationList[fileCounter][0]])
        print(filepathString[combinationList[fileCounter][1]])
        firstTextWindow.delete('1.0', END)
        secondTextWindow.delete('1.0', END)
        with open(filepathString[combinationList[fileCounter][0]], "r") as file:
            firstTextWindow.insert(END, file.read())
        with open(filepathString[combinationList[fileCounter][1]], "r") as file:
            secondTextWindow.insert(END, file.read())

        firstWindowLabel.configure(text=basename(filepathString[combinationList[fileCounter][0]]))
        secondWindowLabel.configure(text=basename(filepathString[combinationList[fileCounter][1]]))

        firstWindowHighlightList = []
        secondWindowHighlightList = []
        number = 1
        #Indentifyin which words is in each cluster
        for val in refinedTFIDFlist:
            for value in sorted(refinedTFIDFlist[val], key=itemgetter(5)):
                if (number != value[5] + 1):
                    print("\n Cluster", value[5] + 1, "includes : \n")
                    number = value[5] + 1
                print(value[1], value[2])
                input = firstTextWindow.get("1.0", END)
                secondWindowInput = secondTextWindow.get("1.0", END)

                firstWindowHighlightList.append([x.start() for x in re.finditer(re.escape(value[1]), input)])
                secondWindowHighlightList.append([x.start() for x in re.finditer(re.escape(value[2]), input)])
                ##Highligthing the words in each cluster
                if (value[3] == combinationList[fileCounter][0] and value[4] == combinationList[fileCounter][1]):
                    for val in FileList[value[3]]:
                        firstWindowPos = 1.0
                        secondWindowPos = 1.0

                        lenValue = val.get(value[1])
                        int(lenValue)

                        RegExpress = r'\W' + re.escape(value[1])
                        for x in range(0, lenValue):

                            firstWindowPos = firstTextWindow.search(RegExpress, firstWindowPos, stopindex=END,
                                                                    regexp=True)
                            firstWindowLastPos = '%s+%dc' % (firstWindowPos, len(value[1]) + 1)
                            firstTextWindow.tag_add("cluster" + str(value[5] + 1), firstWindowPos, firstWindowLastPos)
                            firstWindowPos = firstWindowLastPos

                            firstTextWindow.tag_config("cluster1", foreground='red')
                            firstTextWindow.tag_config("cluster2", foreground='yellow')
                            firstTextWindow.tag_config("cluster3", foreground='blue')
                            firstTextWindow.tag_config("cluster4", foreground='green')
                    for val in FileList[value[4]]:
                        secondWindowPos = 1.0

                        lenValue = val.get(value[2])
                        int(lenValue)

                        RegExpress = r'\W' + re.escape(value[2])
                        for x in range(0, lenValue):

                            secondWindowPos = secondTextWindow.search(RegExpress, secondWindowPos, stopindex=END,
                                                                      regexp=True)
                            secondWindowLastPos = '%s+%dc' % (secondWindowPos, len(value[2]) + 1)
                            secondTextWindow.tag_add("cluster" + str(value[5] + 1), secondWindowPos,
                                                     secondWindowLastPos)
                            secondWindowPos = secondWindowLastPos

                            secondTextWindow.tag_config("cluster1", foreground='red')
                            secondTextWindow.tag_config("cluster2", foreground='yellow')
                            secondTextWindow.tag_config("cluster3", foreground='blue')
                            secondTextWindow.tag_config("cluster4", foreground='green')

    #Creating the start menu
    root = Tk()
    root.title("Plagiarism Detection System")
    titleLabel = Label(root, text="Plagiarism Detection System", font=("Calibri", 20))
    titleLabel.pack()
    QueensLogoImg = PhotoImage(file = os.path.abspath("QueensLogo.png"))
    QueensLogo = Label(root, image=QueensLogoImg, height=200,width=200)
    QueensLogo.pack()
    instructionLabel = Label(root, text="Once Start Has Been Pressed A Directory Will Be Shown To Allow You to Select Files",font=("Calibri", 12))
    instructionLabel.pack()
    startButton = Button(root, text="Start", font=("Calibri", 12), command=openfiledirectory)
    startButton.pack()
    exitButton = Button(root, text="Exit", font=("Calibri", 12), command=closewindowandexitprogram)
    exitButton.pack()
    mainloop()




    #Declaring Variables
    inflectengine = inflect.engine()
    filepathString = root.fileName
    #filepathString = sys.argv[1:]
    filepathCount = len(filepathString)
    fileInfoDictionary = defaultdict(list)
    FileList = defaultdict(list)
    GraphList = defaultdict(list)
    similarityArray = defaultdict(list)
    stringList = defaultdict(list)
    stringFrequency = defaultdict(list)
    refineStringFrequency = defaultdict(list)
    inverseStringFrequency = defaultdict(list)
    refineInverseStringFrequency = defaultdict(list)
    tdidfValue = defaultdict(list)
    unsplitFileList = []
    wordToBeHighlighted = []
    tokenFrequency = []
    finalTokenList = []
    stringLength = []
    testingList = []
    testingList2 = defaultdict(list)
    refinedTFIDFlist = defaultdict(list)
    refinedList = defaultdict(list)
    minMaxTFIDF = []
    clusterArray = defaultdict(list)
    cosineList = defaultdict(list)
    levenshteinAverageList = []
    levenshteinCombinationList = []
    fileCosineValue = []
    fileEuclideanValue = []
    maxTFIDFValue = []
    averageTFIDFValue = []

    print("Files Being Compared :")
    ##Read in all Files which have been selcted by the user and insert them in data frames.
    for y in range(0, filepathCount):
        print(basename(filepathString[y]))
        data = pd.read_fwf(filepathString[y])

        startingDir = 'C:\\'
        fileTypesAllowed = [('Class File', "*.class")]
        title = "Please Choose Files"

        datanew = data.iloc[:, 0]
        datanew = datanew.dropna()

        TotalRows = len(datanew.index)


        lista = []
        unsplitFileList.append(datanew.values)
        for x in range(0, TotalRows):

            lista.extend(datanew.values[x].split(" "))

        countingStrings = Counter(lista)
        countingStringLength = len(lista)
        FileList[y].append(Counter(lista))
        counter = Counter(lista)
        ##First part of TD-IDF
        ##Calculating the term frequency by finding out how many times the word appear and dividing it by the total under of words in document.
        for value in counter:
            stringFrequency[value].append(counter[value] / len(counter))
        testingList.append(counter)

    #Importing the text file which includes of the Java's reserved words.
    with open(os.path.abspath("JavaReservedWords.txt")) as JavaReservedWordsFile:
        JavaReservedWords = JavaReservedWordsFile.readlines()
        #Removing the \n from the reserved words file
        JavaReservedWords = [words.strip() for words in JavaReservedWords]
    ##Second Part of TF-IDF This is the IDF Section
    for x in range(0, len(testingList)):
        for key,value in zip(testingList[x].keys(),testingList[x].values()):
            templist = []
            idfValue = filepathCount / (1 + len(testingList))
            tfvalue = value / len(testingList[x].values())
            templist.append(key)
            ##After IDF has been worked out it is then multiplied by the TF value
            templist.append(tfvalue * idfValue)
            testingList2[x].append(templist)

    #Finding the smallest value to be used as vector length
    #Vectors Must be Exactly the same size
    VectorAmountArray = []
    for x in range(0, len(testingList2)):
        VectorAmountArray.append(len(testingList2[x]))

    VectorArraySize = min(VectorAmountArray)
    print("Vector Array Size :", VectorAmountArray)
    #Vector Size must also be an even number for even number of comparisons to be made
    if(min(VectorAmountArray) & 1):
        VectorArraySize -= 1
    print("Initial Vector Array Size",VectorArraySize)


    #Printing TF-IDF for each word within the file
    for x in range(0, len(testingList2)):
        Vectorcounter = 0
        print("\n")
        for value in sorted(testingList2[x], key=itemgetter(1), reverse=True):

            if (re.search('[a-zA-Z]', value[0]) and value[0] not in JavaReservedWords and Vectorcounter < VectorArraySize):
                print(Vectorcounter, " ) ", value[0], value[1], value)
                Vectorcounter = Vectorcounter + 1
                refinedList[x].append(value)
                cosineList[x].append(value[1])

    #After the values have been checked against the REGEX and the reserved words list, check that the two arrays to check that they are the same size and an even number
    VectorAmountArray = []
    for x in refinedList:
        VectorAmountArray.append(len(refinedList[x]))
    VectorArraySize = min(VectorAmountArray)
    if(min(VectorAmountArray) & 1):
        VectorArraySize -= 1
    #Final Size to be used
    print("New Vector Array Size : ", VectorArraySize)
    #Dropping the elements whcih are no longer needed.
    for x in refinedList:
        refinedList[x] = refinedList[x][:VectorArraySize]
        cosineList[x] = cosineList[x][:VectorArraySize]

    #Creating the combination list based on the number of files
    combinationList = [list(y) for y in itertools.combinations(refinedList, 2)]
    print("Possible Combination of Files ",combinationList)

    #Creating the comparison window
    ApplicationWindow = Tk()
    firstTextWindow = tkScrollable.ScrolledText(ApplicationWindow, height=50, width=110, wrap='word')
    firstTextWindow.grid(row=6, column=0)
    firstTextWindow.bind("<Key>", lambda e: "break")
    firstWindowButton = Button(ApplicationWindow, text ="Previous", command=decrementview)
    firstWindowButton.grid(row=7, column=0)
    firstWindowLabel = Label(ApplicationWindow, text=basename(filepathString[0]),font=("Calibri", 12))
    firstWindowLabel.grid(row=5, column=0)
    #Legend Labelling
    legendLabel = Label(ApplicationWindow, text="Legend :")
    legendLabel.grid(row=0, column=0, sticky=W)
    cluster1LegendLabel = Label(ApplicationWindow, text="Cluster 1 = Red")
    cluster1LegendLabel.configure(foreground="red")
    cluster1LegendLabel.grid(row=1, column=0, sticky=W)
    cluster2LegendLabel = Label(ApplicationWindow, text="Cluster 2 = Yellow")
    cluster2LegendLabel.configure(foreground="Yellow")
    cluster2LegendLabel.grid(row=2, column=0, sticky=W)
    cluster3LegendLabel = Label(ApplicationWindow, text="Cluster 3 = Blue")
    cluster3LegendLabel.configure(foreground="Blue")
    cluster3LegendLabel.grid(row=3, column=0, sticky=W)
    cluster4LegendLabel = Label(ApplicationWindow, text="Cluster 4 = Green")
    cluster4LegendLabel.configure(foreground="Green")
    cluster4LegendLabel.grid(row=4, column=0, sticky=W)
    #Loading Initial Values into File
    with open(filepathString[0], "r") as file:
        firstTextWindow.insert(END, file.read())
    secondWindowLabel = Label(ApplicationWindow, text=basename(filepathString[1]),font=("Calibri", 12))
    secondWindowLabel.grid(row=5, column=1)
    secondTextWindow = tkScrollable.ScrolledText(ApplicationWindow, height=50, width=110, wrap='word')
    secondTextWindow.grid(row=6, column=1)
    secondTextWindow.bind("<Key>", lambda e: "break")
    secondWindowButton = Button(ApplicationWindow, text ="Next", command=incrementview)
    secondWindowButton.grid(row=7, column=1)
    NextScreenButton = Button(ApplicationWindow, text="Finish", command=FinalWindow)
    NextScreenButton.grid(row=8, column=1, sticky=E)
    with open(filepathString[1], "r") as file:
        secondTextWindow.insert(END, file.read())
    #Since we can repersent all words as TF-IDF Values we can now take advantage of similarity techniques such as cosine and euclidean comparison
    #Cosine / Euclidean Comparison
    for x in combinationList:
        print("\n" + "Cosine Value For Files ", basename(filepathString[x[0]]), " and ",
              basename(filepathString[x[1]]))
        print(spatial.distance.cosine(cosineList[x[0]],cosineList[x[1]]))
        fileCosineValue.append(spatial.distance.cosine(cosineList[x[0]],cosineList[x[1]]))
        fileEuclideanValue.append(spatial.distance.euclidean(cosineList[x[0]],cosineList[x[1]]))

    comboCounter = 0

    clusteringList = []
    #For Loop which will create the refinedTF-IDF List
    #This list will include TF-IDF values, Word, TF-IDF Difference and File Numbers
    for x in combinationList:
        print("Refined List",refinedList[x[0]])
        for value1, value2 in zip(refinedList[x[0]], refinedList[x[1]]):
            print(value1[0],value1[1],value2[0], value2[1],abs(value1[1] - value2[1]), x[0], x[1])
            tempclusteringList = []
            tempclusteringList.append(value1[1])
            tempclusteringList.append(value2[1])
            TDIDFtemplist = []
            TDIDFtemplist.append(abs(value1[1] - value2[1]))
            TDIDFtemplist.append(value1[0])
            TDIDFtemplist.append(value2[0])
            TDIDFtemplist.append(x[0])
            TDIDFtemplist.append(x[1])
            refinedTFIDFlist[comboCounter].append(TDIDFtemplist)
            clusteringList.append(tempclusteringList)
        comboCounter = comboCounter + 1
    #K Means Clustering
    #4 Clusters being created and each vector is being assigned to a cluster.
    #Using K Means Clustering to cluster our clustering list
    kmeansClustering = KMeans(n_clusters=4,random_state=0).fit_predict(clusteringList)
    print("KmeansClustering", kmeansClustering)
    print("KmeansClustering", len(kmeansClustering))
    # Cluster number is then being assign to refindTFIDTlist
    print("TF-IDF Value, File 1 Word, File 2 Word, File Number, File Number, Cluster Number")

    for val in refinedTFIDFlist:
        print("Refined TF-IDF List with the Cluster Append to List")
        for value,value1 in zip(refinedTFIDFlist[val],kmeansClustering):
            value.append(value1)
            print(value)


    for x in refinedTFIDFlist:
        for y in range(0, len(refinedTFIDFlist[x]),2):
            list1 = refinedTFIDFlist[x][y]
            list2 = refinedTFIDFlist[x][y+1]
            print(y, list1, list2)
            minMaxTFIDF.append(list1[0])
            minMaxTFIDF.append(list2[0])

    print("Min",min(minMaxTFIDF))
    print("Max",max(minMaxTFIDF))

    # ##Create a refined list of stringFrequency by removing anything which doesnt appear in both of the documents.
    # ##This is being done by checking the length of stringFrequency
    # for v in stringFrequency:
    #     refineStringFrequency[v].append(stringFrequency[v])
    #
    #
    # #Capturing All Values which have been renamed. Only values which have a similarity Ratio of > 0.5 will be considered.
    # ##This has been put in place so that renamed words will still be considered for exam output will be compared to outputString
    # counterv = 0
    # for value in list(FileList):
    #     list1 = FileList[value]
    #     list2 = FileList[value+1]
    #     for key,val in zip(list1, list2):
    #         for k, v in zip(key, val):
    #             if (SequenceMatcher(None, k, v).ratio() > 0.5 and SequenceMatcher(None, k, v).ratio() < 1):
    #                 print(k,v)
    #                 if not(v in refineStringFrequency.keys()):
    #                     #print(k, v)
    #                     listtemp = []
    #                     listtemp.append(key.get(k)/len(key))
    #                     listtemp.append(val.get(v)/len(val))
    #                     counterv = counterv + 1
    #                     countervalue = len(refineStringFrequency) + counterv
    #                     refineStringFrequency[v].append(listtemp)
    #
    #
    # ##This for loop is calculating the inverse document frequency which is taking the total number of files and dividing by the number of files which the word appears in.
    # ##Plus 1 has been added so if a file appears in 0 documents, you are not dividing by zero.
    # for refined in refineStringFrequency:
    #     #print(inverseStringFrequency[refined])
    #     for refine in refineStringFrequency[refined]:
    #         for q in range(0, len(refine)):
    #             value = filepathCount / (1 + len(stringFrequency[v]))
    #             tdidfValue[refined].append(refine[q] * value)
    #
    # ##Output Statements
    # ##Displaying the total amount of matches
    # print("There is a total of", len(tdidfValue), "matches in the", filepathCount, "files")
    #
    #

    counter = 0
    ratioArray = []
    firstWindowHighlightList = []
    secondWindowHighlightList = []
    number = 1
    ##Highlighting the Initial two class files
    for val in refinedTFIDFlist:
        for value in sorted(refinedTFIDFlist[val], key=itemgetter(5)):
            if(number != value[5]+1):
                number = value[5]+1

            clusterTemp = []
            clusterTemp.append(value[1])
            clusterTemp.append(value[2])
            clusterTemp.append(value[3])
            clusterTemp.append(value[4])
            clusterArray[value[5]+1].append(clusterTemp)
            print(value[1], value[2])
            input = firstTextWindow.get("1.0",END)
            secondWindowInput = secondTextWindow.get("1.0", END)

            if(value[3] == 0 and value[4] == 1):
                #Highlight the Left Class
                for val in FileList[value[3]]:
                    firstWindowPos = 1.0
                    lenValue = val.get(value[1])
                    int(lenValue)
                    RegExpress = r'\W' + re.escape(value[1])
                    for x in range(0, lenValue):
                        firstWindowPos = firstTextWindow.search(RegExpress, firstWindowPos, stopindex=END, regexp=True)
                        firstWindowLastPos = '%s+%dc' % (firstWindowPos, len(value[1])+1)
                        firstTextWindow.tag_add("cluster" + str(value[5] + 1), firstWindowPos, firstWindowLastPos)
                        firstWindowPos = firstWindowLastPos

                        firstTextWindow.tag_config("cluster1", foreground='red')
                        firstTextWindow.tag_config("cluster2", foreground='yellow')
                        firstTextWindow.tag_config("cluster3", foreground='blue')
                        firstTextWindow.tag_config("cluster4", foreground='green')
                #Highlighting the Right Class
                for val in FileList[value[4]]:
                    secondWindowPos = 1.0
                    lenValue = val.get(value[2])
                    int(lenValue)
                    RegExpress = r'\W' + re.escape(value[2])
                    for x in range(0, lenValue):
                        secondWindowPos = secondTextWindow.search(RegExpress, secondWindowPos, stopindex=END,regexp=True)
                        secondWindowPos = secondTextWindow.search(RegExpress, secondWindowPos, stopindex=END,regexp=True)
                        secondWindowLastPos = '%s+%dc' % (secondWindowPos, len(value[2]) + 1)
                        secondTextWindow.tag_add("cluster" + str(value[5] + 1), secondWindowPos, secondWindowLastPos)
                        secondWindowPos = secondWindowLastPos

                        secondTextWindow.tag_config("cluster1", foreground='red')
                        secondTextWindow.tag_config("cluster2", foreground='yellow')
                        secondTextWindow.tag_config("cluster3", foreground='blue')
                        secondTextWindow.tag_config("cluster4", foreground='green')

    print("CLUSTER",clusterArray)
    newList = []
    ## Method Token Analysis
    ##Using Regex to find all methods within the class
    strTest = str(unsplitFileList)
    listString = ""
    rgxstr = "\{(.*?)\}"
    for rgxV in range(0, len(unsplitFileList)):
        listString = ''.join(unsplitFileList[rgxV])
        rgxString = re.findall(rgxstr, listString)
        newList.append(rgxString)

    lexicalAnalysisList = []
    lexString = ""
    lisTest = []
    ##Changing String to repersent token
    for listSize in newList:
        #print(listSize)
        for val in listSize:
            #print(val)
            for char in val:
                if char.isalpha():
                    lexString = lexString + "A"
                if char.isdigit():
                    lexString = lexString + "D"
                if char.isspace():
                    lexString = lexString + "W"
                if char.isalnum() == False and char.isspace() == False:
                    lexString = lexString + "S"
            lexicalAnalysisList.append(lexString)
            lexString = ""
        lisTest.append(lexicalAnalysisList)
        lexicalAnalysisList = []

    refinedMethodList = defaultdict(list)
    for x in range(0, len(lisTest)):
        for value, value1 in zip(lisTest[x], newList[x]):
            temp = []
            temp.append(value)
            temp.append(value1)
            refinedMethodList[x].append(temp)
            refinedMethodList[x].append(temp)
    print("MethodList", refinedMethodList)
    for x in combinationList:
        ##Generate All Combinations of methods
        methodCombo = list(itertools.product(refinedMethodList[x[0]], refinedMethodList[x[1]]))
        print("\n" + "Comparing Methods of Files : ", basename(filepathString[x[0]]), " and ",
              basename(filepathString[x[1]]))
        levenshteinAvgTemp = []
        cosineMethodAvgTemp = []
        for value in methodCombo:
            ##Only Consider Levenshtein values which is greater than 0.80
            ##Not every combination is going to have meaning so only consider anything above 0.80
            if (Levenshtein.ratio(value[0][0], value[1][0]) > 0.80):
                print("\n" + "Levenshtein Value : ", Levenshtein.ratio(value[0][0], value[1][0]))
                print("Tokens :")
                print(value[0][0])
                print(value[1][0])
                print("Method String :")
                print(value[0][1])
                print(value[1][1])
                #cosineMethodAvgTemp.append(spatial.distance.cosine(value[0][0],value[1][0]))
                levenshteinAvgTemp.append(Levenshtein.ratio(value[0][0], value[1][0]))
        print("Average Levenshtein value for files: ", basename(filepathString[x[0]]), " and ",basename(filepathString[x[1]]), " is ", np.mean(levenshteinAvgTemp))
        levenshteinAverageList.append(np.mean(levenshteinAvgTemp))
        levenshteinCombinationList.append(len(levenshteinAvgTemp))
        print(len(levenshteinAvgTemp))

    for x in unsplitFileList:
        for c in x:
            if(len(c) > 1):
                stringLength.append(len(c))

    totaltime = time.time() - startTime
    ##Printing Final Execution Time
    print("Execution Time : ", time.time() - startTime)
    print(os.path.abspath("ExecutionTime.txt"))
    executionTimeFile = open(os.path.abspath("ExecutionTime.txt"), "a")

    executionTimeFile.write("Execution Time : " + str(totaltime) + "\n")
    executionTimeFile.close()
    mainloop()
##Recording Any Errors Which Occur to File
except Exception as exception:
    print("Error Logged to File ExceptionsRecord.txt")
    with open(os.path.abspath("ExceptionsRecord.txt"), "a") as ExceptionFile:
        ExceptionFile.write(traceback.format_exc() + "\n")
